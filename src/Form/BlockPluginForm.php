<?php
use Drupal\Core\Form\FormBase;

/**
 * Created by PhpStorm.
 * User: student
 * Date: 12/22/16
 * Time: 4:11 PM
 */
class BlockPluginForm extends FormBase {


    public function getFormId()
    {
        return "loremipsum_block_form";
    }

    /**
     * Form constructor.
     *
     * @param array $form
     *   An associative array containing the structure of the form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current state of the form.
     *
     * @return array
     *   The form structure.
     */
    public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state)
    {

        $form['paragraphs'] = [
            '#type' => 'select',
            '#default-value'=> '3',
            '#title' => $this->t('Paragraf'),
            '#options' => [
                '1' => $this->t('One'),
                '2' => $this->t('Two'),
                '3' => $this->t('Three'),
            ],
        ];
        $form['phrases'] = [
            '#type' => 'textfield',
            'default-value'=>'20',
            '#title' => $this->t('Phrases'),
            '#size' => 60,
            '#maxlength' => 128
        ];
        $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Generate'),
        ];

 // @endcode

 //* @FormElement("select")
    }

    /**
     * Form submission handler.
     *
     * @param array $form
     *   An associative array containing the structure of the form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current state of the form.
     */
    public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state)
    {
        $form_state->setRedirect(
            'loremipsum.generate',
            array(
                'paragraphs' => $form_state->getValue('paragraphs'),
                'phrases' => $form_state->getValue('phrases'),
            ));
    }
    public function validateForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state)
    {
        parent::validateForm($form, $form_state); //

    }
}