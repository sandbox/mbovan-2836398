<?php

namespace Drupal\loremipsum;

/**
 * Interface LoremIpsumControllerInterface.
 *
 * @package Drupal\loremipsum
 */
interface LoremIpsumControllerInterface {

  /**
   * Returns the render array with our LoremIpsum text.
   *
   * Constructs Lorem ipsum text with arguments.
   * This callback is mapped to the path
   * 'loremipsum/generate/{paragraphs}/{phrases}'.

   * @param string $paragraphs
   *   The amount of paragraphs that need to be generated.
   * @param string $phrases
   *   The maximum amount of phrases that can be generated inside a paragraph.
   *
   * @return array
   *   Returns the render array
   */
  public function generate($paragraphs, $phrases);

}
