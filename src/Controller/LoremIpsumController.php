<?php
namespace Drupal\loremipsum\Controller;
use Drupal\loremipsum\LoremIpsumControllerInterface;

class LoremIpsumController implements LoremIpsumControllerInterface
{

  /**
   * @inheritDoc
   */
  public function generate($paragraph, $phrases)
  {
    $settings = \Drupal::config("loremipsum.settings");
    $page_title = $settings->getRawData()['loremipsum']['page_title'];
    $source_text = $settings->getRawData()['loremipsum']['source_text'];
    $source_text_array = [];
    for ($i = 0; $i < $paragraph; $i++) {
      $source_text_array[] = $source_text;
    }
    return [
      '#theme' => 'loremipsum',
      '#source_text' => $source_text_array,
      '#title' => $page_title,
    ];
  }



}
